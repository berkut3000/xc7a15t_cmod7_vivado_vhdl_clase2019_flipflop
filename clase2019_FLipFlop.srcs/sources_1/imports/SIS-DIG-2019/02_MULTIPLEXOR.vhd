----------------------------------------------------------------------------------
-- JORGE SOTO
-- Programa para demostrar el uso de Multiplexores del FPGA
----------------------------------------------------------------------------------
-- Librerias:
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity MULTIPLEXOR is
	port(	DIN : in std_logic_vector (31 downto 0);
			SEL : in std_logic_vector (4 downto 0);
			S : out std_logic);			
end MULTIPLEXOR;

architecture Behavioral of MULTIPLEXOR is

begin
-- Forma sugerida en VHDL: 4 SLICES S3, 1 SLICE S6
--process(DIN, SEL) begin	
--	case SEL is
--		when "0000" => S <= DIN(0);
--		when "0001" => S <= DIN(1);
--		when "0010" => S <= DIN(2);
--		when "0011" => S <= DIN(3);
--		when "0100" => S <= DIN(4);
--		when "0101" => S <= DIN(5);
--		when "0110" => S <= DIN(6);
--		when "0111" => S <= DIN(7);
--		when "1000" => S <= DIN(8);
--		when "1001" => S <= DIN(9);
--		when "1010" => S <= DIN(10);
--		when "1011" => S <= DIN(11);
--		when "1100" => S <= DIN(12);
--		when "1101" => S <= DIN(13);
--		when "1110" => S <= DIN(14);
--		when others => S <= DIN(15);
--	end case;

-- Forma practica: 4 SLICES S3, 1 SLICE S6, A7
S <= DIN(conv_integer(SEL));

end Behavioral;

